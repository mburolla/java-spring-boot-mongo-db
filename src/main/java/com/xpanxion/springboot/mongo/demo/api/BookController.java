package com.xpanxion.springboot.mongo.demo.api;

import org.springframework.web.bind.annotation.*;
import com.xpanxion.springboot.mongo.demo.models.Book;
import org.springframework.beans.factory.annotation.Autowired;
import com.xpanxion.springboot.mongo.demo.repository.BookRepository;

@RestController
public class BookController {

    @Autowired
    private BookRepository bookRepository;

    @GetMapping("api/v1/books/{id}")
    public Book getBook(@PathVariable int id) {
        var optBook = bookRepository.findById(id);
        return optBook.get();
    }

    @PostMapping("api/v1/books")
    public String addBook(@RequestBody Book book) {
        bookRepository.save(book);
        return "Added book: " + book.getName();
    }
}
