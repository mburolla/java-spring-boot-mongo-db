package com.xpanxion.springboot.mongo.demo.repository;

import com.xpanxion.springboot.mongo.demo.models.Book;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BookRepository extends MongoRepository <Book, Integer> { }
